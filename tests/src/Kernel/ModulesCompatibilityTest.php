<?php

declare(strict_types=1);

namespace Drupal\Tests\wse\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests modules' compatibility with each other.
 *
 * @group wse
 */
class ModulesCompatibilityTest extends KernelTestBase {

  /**
   * Tests for hidden dependencies and un/installation issues.
   *
   * Also (basically) confirms that dependencies are properly declared. Note:
   * this may seem like a silly test, but it immediately caught bugs when
   * it was written.
   *
   * @dataProvider providerInstallUninstallModules
   */
  public function testInstallUninstallModules(array $modules): void {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
    $module_installer = $this->container->get('module_installer');

    // Install the modules.
    try {
      $module_installer->install($modules);
    }
    catch (\Throwable $e) {
      self::fail(
        sprintf(
          'Error installing modules: %s',
          $e->getMessage()
        )
      );
    }
    foreach ($modules as $module) {
      self::assertTrue(
        \Drupal::moduleHandler()->moduleExists($module),
        sprintf(
          'Module %s was installed',
          $module
        )
      );
    }

    // Uninstall the modules.
    try {
      $module_installer->uninstall($modules);
    }
    catch (\Throwable $e) {
      self::fail(
        sprintf(
          'Error uninstalling modules: %s',
          $e->getMessage()
        )
      );
    }
    foreach ($modules as $module) {
      self::assertFalse(
        \Drupal::moduleHandler()->moduleExists($module),
        sprintf(
          'Module %s was uninstalled',
          $module
        )
      );
    }
  }

  /**
   * Provides data for testInstallUninstallModules().
   *
   * @return array
   *   An array of module lists.
   */
  public static function providerInstallUninstallModules(): array {
    return [
      'wse' => [['wse']],
      'wse_menu' => [['wse_menu']],
      'wse_lb' => [['wse_lb']],
      'wse_config' => [['wse_config']],
      'wse_scheduler' => [['wse_scheduler']],
      'wse_preview' => [['wse_preview']],
      'wse_group_access' => [['wse_group_access']],
      'all' => [
        [
          'wse',
          'wse_menu',
          'wse_lb',
          'wse_config',
          'wse_scheduler',
          'wse_preview',
          'wse_group_access',
        ],
      ],
    ];
  }

}
